# Open Weather API test automation challenge

## Tools for testing

- Java language, jdk1.8.0_231
- IDE: IntelliJ.
- Project management and construction: Maven
- Test Framework: TestNG.
- testing of REST services: Rest assured library
- Get information from Json: Json Path Library

## Steps to Run Project

-	Have java 8 or higher installed.
-	Use or download Intellij Community Edition.
-	Open project in IDE.

## Identify Type of Project

-	At the root of the Project we right click Add Framework support and select Maven
-	Run GetRequestWeather class.

# Project Structure

###	Inside the package APITesting.com.org.api we have the class GetRequestWeather with different test cases:

* **Test_get_01:** Check if we get a 200 status code by getting weather entering a valid city name (“Provincia de Zaragoza”) with a valid appid.
* **Test_get_02:** Check if we get a 401 status code by getting weather entering a valid city name (“Provincia de Zaragoza”) with an invalid appid.
* **Test_get_03:** Check if we get 200 status code by getting weather using valid parameters of city name (“London”) with a valid appid.
* **Test_get_04 **and Test_get_05: Check if we get 200 status code by getting weather using valid parameters of city name (“London”) with a valid appid and using rest assured library.
* **Test_get_06:** Check if we get 200 status code by getting weather using valid parameters of city id (“3104323”) with a valid appid and displaying the result of the response in console.
* **Test_get_07:** Check if we get 200 status code by getting weather using valid parameters of city zip code (“50009,es”) with a valid appid and displaying the result of the response in console.
* **Test_get_08:** Check information by getting weather using valid parameters of city id (“3104323”) with a valid appid and displaying the result getting specific weather description from json path.
* **Test_get_09:** Check if test pass by getting weather using valid parameters of city id (“3104323”) with a valid appid and displaying the result getting specific weather description from json path and comparing the value obtained with the value of a given variable (this value can also come from a database data).
* **Test_get_10:** Compare results when getting weather description with valid city id and appid and get weather description passing longitude and latitude parameters and valid appid.
