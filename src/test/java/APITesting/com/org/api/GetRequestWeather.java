package APITesting.com.org.api;

import com.jayway.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.restassured.response.Response;
import static com.jayway.restassured.RestAssured.*;

public class GetRequestWeather {

    //Get Request by city name
    @Test
    public void Test_get_01() {

        Response resp = when().
                get("https://api.openweathermap.org/data/2.5/weather?q=Provincia de Zaragoza,es&appid=91cb14762d932dca7f461dae39cd7960");

        System.out.println(resp.getStatusCode());
        Assert.assertEquals(resp.getStatusCode(), 200);
    }

    //Status code: 401
    @Test
    public void Test_get_02() {

        Response resp = when().
                get("https://api.openweathermap.org/data/2.5/weather?q=Provincia de Zaragoza,es&appid=91cb1ca761dae39cd7960");

        System.out.println(resp.getStatusCode());
        Assert.assertEquals(resp.getStatusCode(), 401);
    }

    //Using parameters with rest assured
    @Test
    public void Test_get_03() {

        Response resp = given()
                .param("q", "London")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather");

        System.out.println(resp.getStatusCode());
        Assert.assertEquals(resp.getStatusCode(), 200);

        if (resp.getStatusCode() == 200) {
            System.out.println("API is working fine");
        } else {
            System.out.println("Api is not working fine");
        }
    }

    //Assert our testcase in Rest assured api
    @Test
    public void Test_get_04() {

        given().
                param("q", "London").
                param("appid", "91cb14762d932dca7f461dae39cd7960").
                when().
                get("https://api.openweathermap.org/data/2.5/weather").
                then().
                assertThat().statusCode(200);


    }

    //Using parameters with rest assured
    @Test
    public void Test_get_05() {

        Response resp = given()
                .param("q", "Provincia de Zaragoza")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather");

        System.out.println(resp.asString());

    }

    //Get Request by city id
    @Test
    public void Test_get_06() {

        Response resp = given()
                .param("id", "3104323")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather");

        Assert.assertEquals(resp.getStatusCode(),200);
        System.out.println(resp.asString());
    }

    //Get Request by city zip, Zaragoza spain
    @Test
    public void Test_get_07() {

        Response resp = given()
                .param("zip", "50009,es")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather");

        Assert.assertEquals(resp.getStatusCode(),200);
        System.out.println(resp.asString());
    }

    //Getting info from json path
    @Test
    public void Test_get_08() {

        String weatherReport = given()
                .param("id", "3104323")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather").
                        then().
                        contentType(ContentType.JSON).
                        extract().
                        path("weather[0].description");

        System.out.println("Wheather report: " + weatherReport);
    }

    //Getting info from json path
    @Test
    public void Test_get_09() {

        Response resp = given()
                .param("id", "3104323")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather");

         String actualWeatherReport = resp.
                        then().
                        contentType(ContentType.JSON).
                        extract().
                        path("weather[0].description");

         String expectedWeatherReport = "clear sky";

         if (actualWeatherReport.equalsIgnoreCase(expectedWeatherReport)){
             System.out.println("TestCase pass");
         }
         else
             System.out.println("Testcase fail because current weather is: "+ actualWeatherReport);
    }

    //Getting latitude and longitude from json path
    @Test
    public void Test_get_10() {

        Response resp = given()
                .param("id", "3104323")
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather");

        String reportbyID = resp.
                then().
                contentType(ContentType.JSON).
                extract().
                path("weather[0].description");


        System.out.println("Weather description by ID: " + reportbyID);

        String lon = resp.
                then().
                contentType(ContentType.JSON).
                extract().
                path("coord.lon").toString();

        System.out.println("Longitude is: " + lon);

        String lat = resp.
                then().
                contentType(ContentType.JSON).
                extract().
                path("coord.lat").toString();

        System.out.println("Latitude is: " + lat);

        String reportByCoordinates = given()
                .param("lat", lat)
                .param("lon", lon)
                .param("appid", "91cb14762d932dca7f461dae39cd7960").
                        when().
                        get("https://api.openweathermap.org/data/2.5/weather").
                        then().
                        contentType(ContentType.JSON).
                        extract().
                        path("weather[0].description");


        System.out.println("Report by coordinates " + reportByCoordinates);
        Assert.assertEquals(reportbyID, reportByCoordinates);


    }

}
